import styled from "styled-components/native"

export const KeyboardView = styled.KeyboardAvoidingView `
    flex: 1;
    align-items: center;
    justify-content: center;
    background-color: #EFECF8;
    padding-bottom: 30px;
`
export const Container = styled.View`
    flex: 1;
    justify-content:top;
    align-items: center;
    padding-bottom:30px;
    width: 90%;

`

export const Title = styled.Text`
    color: #000;
    font-size: 25px;
    font-weight: 700;
    margin-bottom: 5px;
    max-height: 50px;
    margin-top: 2%;

`

export const Input = styled.TextInput`
    background-color: #fff;
    margin-bottom: 20px;
    padding: 10px 20px;
    color: #000;
    font-size: 20px;
    border-radius: 10px;
    width: 90%;


    
`

export const ButtonSubmit = styled.TouchableOpacity`
    background-color: #A287f4;
    border-radius: 10px;
    align-items: center;
    padding: 10px 20px;
    width: 90%;
    margin-bottom: 30px;
`

export const TextButton = styled.Text`
    color: #fff;
    font-size: 20px;
    font-weight: bold;
`
export const Teste = styled.Text`
    color: #000;
    font-size: 25px;
    flex: 1;
    text-align: center;
    padding-top: 10px;
    font-weight: bold;
    width: 95%;
    max-height: 20%;
    min-height: 35%;

`