import React from 'react'
import {KeyboardView, 
        Container,
        Title,  
        Input,
        ButtonSubmit,
        TextButton, Teste } from './styles'

import Header from '../../components/Header/Header'

const Login = () => {

  return (
    <KeyboardView behavior= {(Platform.OS === 'ios')? "padding" : null}>
        <Header/>
        <Container>
        <Teste>Olá, seja bem vindo ao PsicoApp, faça o login para entrar.</Teste>
          <Title>Login</Title>
          <Input placeholderTextColor='#000'
                  placeholder='Email'/>
          <Input placeholderTextColor='#000'
                  placeholder='Senha'/>
          <ButtonSubmit>
            <TextButton>Entrar</TextButton>
          </ButtonSubmit>
        </Container>
    </KeyboardView>
  )
}

export default Login
