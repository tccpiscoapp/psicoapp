import styled from "styled-components";

export const Container = styled.View`
    margin-top: 10%;
    padding: 10px;

`
export const Teste = styled.Text`
    color: #000;
    font-size: 25px;
    flex: 1;
    text-align: center;
    padding-top: 10px;
    font-weight: bold;
    width: 95%;

    background-color: red;
`