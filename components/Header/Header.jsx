import React from 'react'
import { Image, Text } from 'react-native'
import { Container, Teste } from './styles'
import Logo from '../../assets/logotcc.png'

const Header = () => {
  return (
    <>
    <Container>
        <Image style={{width:100, height:100, padding:40}} source={Logo}/>
        <Text style={{textAlign:'center',fontWeight:'bold'}}>PSICOAPP</Text>
    </Container>
    </>

  )
}

export default Header